import cors from "cors";

export const appCors = (): any => {
  return cors({
    credentials: true,
    origin: "http://localhost:3000",
  });
};
