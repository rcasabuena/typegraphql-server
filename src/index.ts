import "reflect-metadata";
import { ApolloServer } from "apollo-server-express";
import express from "express";
import { buildSchema } from "type-graphql";
import { createConnection } from "typeorm";
import { appSession } from "./session";
import { appCors } from "./cors";

const main = async () => {
  await createConnection();

  const schema = await buildSchema({
    resolvers: [__dirname + "/modules/**/*.ts"],
    authChecker: ({ context: { req } }) => {
      return !!req.session.userId;
    },
  });

  const apolloServer = new ApolloServer({
    schema,
    context: ({ req, res }: any) => ({ req, res }),
  });

  const app = express();

  app.use(appCors());

  app.use(await appSession());

  apolloServer.applyMiddleware({ app });

  app.listen(process.env.PORT || 3000, () => {
    console.log(`server listening in port ${process.env.PORT}`);
  });
};

main();
