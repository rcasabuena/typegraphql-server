import { Arg, Mutation, Resolver } from "type-graphql";
import { User } from "../../entity/User";
import { redis } from "../../redis";

@Resolver()
export class ConfirmUserResolver {
  @Mutation(() => Boolean)
  async confirmUser(@Arg("token") token: string): Promise<boolean> {
    const userId = await redis.get(`confirm-token:${token}`);
    console.log(userId);
    if (!userId) return false;

    await User.update({ id: userId }, { confirmed: true });
    await redis.del(`confirm-token:${token}`);

    return true;
  }
}
