import { Arg, Mutation, Resolver } from "type-graphql";
import { User } from "../../entity/User";
import { createForgotPasswordUrl } from "../utils/createForgotPasswordUrl";
import { sendEmail } from "../utils/sendEmail";

@Resolver()
export class ForgotPasswordResolver {
  @Mutation(() => Boolean)
  async forgotPassword(@Arg("email") email: string): Promise<boolean> {
    const user = await User.findOne({ where: { email } });

    if (user) {
      const url = await createForgotPasswordUrl(user.id);

      const emailOptions = {
        from: '"Typegraphql API" <noreply@typegraphqlapi.co>', // sender address
        to: "ritchie.casabuena@gmail.com, ritchie.casabuena+1@gmail.com", // list of receivers
        subject: "Password reset your API account", // Subject line
        //text: `${url}`, // plain text body
        html: `<a href="${url}">${url}</a>`, // html body
      };

      await sendEmail(emailOptions);
    }

    return true;
  }
}
