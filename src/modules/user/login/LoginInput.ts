import { IsEmail } from "class-validator";
import { PasswordInput } from "../../shared/PasswordInput";
import { Field, InputType } from "type-graphql";

@InputType()
export class LoginInput extends PasswordInput {
  @Field()
  @IsEmail()
  email: string;
}
