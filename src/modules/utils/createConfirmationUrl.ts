import { redis } from "../../redis";
import { v4 } from "uuid";

export const createConfirmationUrl = async (
  userId: string
): Promise<string> => {
  const id = v4();
  await redis.set(`confirm-token:${id}`, userId, "ex", 60 * 60 * 24);
  return `${process.env.FRONTEND_BASEURL}/user/confirm/${id}`;
};
