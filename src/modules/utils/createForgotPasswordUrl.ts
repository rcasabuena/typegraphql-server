import { redis } from "../../redis";
import { v4 } from "uuid";

export const createForgotPasswordUrl = async (
  userId: string
): Promise<string> => {
  const id = v4();
  await redis.set(`forgot-password-token:${id}`, userId, "ex", 60 * 60 * 24);
  return `${process.env.FRONTEND_BASEURL}/user/change-password/${id}`;
};
