import nodemailer from "nodemailer";
import nodemailerSendgrid from "nodemailer-sendgrid";

interface EmailOptions {
  from: string;
  to: string;
  subject: string;
  text?: string;
  html: string;
}

export async function sendEmail(emailOptions: EmailOptions) {
  const transporter = nodemailer.createTransport(
    nodemailerSendgrid({
      apiKey: process.env.SENDGRID_API_KEY,
    })
  );

  const info = await transporter.sendMail(emailOptions);

  console.log(info);
}
