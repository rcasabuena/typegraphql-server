const { Interface } = require("readline");

import { Request, Response } from "express";

export interface MyContext {
  req: Request;
  res: Response;
}
