import session from "express-session";
import connectRedis from "connect-redis";
import { redis } from "./redis";

declare module "express-session" {
  export interface SessionData {
    userId: string;
  }
}

export const appSession = async (): Promise<any> => {
  const RedisStore = connectRedis(session);

  return session({
    store: new RedisStore({
      client: redis,
    }),
    name: "qid",
    secret: process.env.SESSION_SECRET!,
    resave: false,
    saveUninitialized: false,
    cookie: {
      httpOnly: true,
      secure: process.env.NODE_ENV === "production",
      maxAge: 1000 * 60 * 60 * 24 * 180, // 6 months
    },
  });
};
